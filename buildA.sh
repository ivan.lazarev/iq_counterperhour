DEPENDENCY_URL=$1
DEPENDENCY_DIR=$2
COMPILE_DIR=$3

echo checking out dependency ${DEPENDENCY_DIR}

#git clone https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/${GITLAB_USER}/iq_Log
git clone ${DEPENDENCY_URL} ${DEPENDENCY_DIR}
ls -la ./iq_Log

echo building dependency ${DEPENDENCY_DIR}

javac ./iq_Log/src/*.java -d ${COMPILE_DIR}
ls -l ${COMPILE_DIR}
