import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        CounterPerHour counter = new CounterPerHour();
        List<CounterThread> threads = new ArrayList<CounterThread>();

        // start all
        for (int i = 0; i < 500; i++) {
            CounterThread ct = new CounterThread(counter, i+"");
            threads.add(ct);
            ct.start();
        }

        //wait all
        for (CounterThread thread : threads) {
            thread.join();
        }

        System.out.println("Last hour: " + counter.getPerHour());
    }
}

class CounterThread extends Thread {

    private CounterPerHour counter;

    public CounterThread(CounterPerHour counter, String name) {
        this.setName(name);
        this.counter = counter;
    }

    @Override
    public void run() {
        for (int i = 0; i < 1000; i++) {
            counter.add();
        }
        System.out.println(this.getName());
    }
}